namespace: product.openurl
flow:
  name: open_url2_flow
  workflow:
    - open_url2:
        do:
          product.openurl.open_url2: []
        navigate:
          - SUCCESS: SUCCESS
          - WARNING: SUCCESS
          - FAILURE: on_failure
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      open_url2:
        x: 125
        'y': 179
        navigate:
          86b87c1d-0d2e-6fa3-a202-2ed009a5af0b:
            targetId: bbff9a2c-b89b-43f2-c4bb-4ccbc97fccc2
            port: SUCCESS
          b70e9f02-d0f9-44c9-806b-6d50d657fb83:
            targetId: bbff9a2c-b89b-43f2-c4bb-4ccbc97fccc2
            port: WARNING
    results:
      SUCCESS:
        bbff9a2c-b89b-43f2-c4bb-4ccbc97fccc2:
          x: 333
          'y': 153
