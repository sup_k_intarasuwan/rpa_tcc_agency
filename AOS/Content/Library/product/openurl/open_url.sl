namespace: product.openurl
operation:
  name: open_url
  inputs:
  - URL
  sequential_action:
    gav: com.microfocus.seq:product.openurl.open_url:1.0.0
    external: true
  outputs:
  - return_result: ${return_result}
  - error_message: ${error_message}
  results:
  - SUCCESS
  - WARNING
  - FAILURE
