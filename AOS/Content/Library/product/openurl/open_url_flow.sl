namespace: product.openurl
flow:
  name: open_url_flow
  inputs:
    - url: www.google.co.th
  workflow:
    - open_url:
        do:
          product.openurl.open_url:
            - URL: '${url}'
        navigate:
          - SUCCESS: SUCCESS
          - WARNING: SUCCESS
          - FAILURE: on_failure
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      open_url:
        x: 260
        'y': 197
        navigate:
          8bf0bada-b6c1-47d8-b8b0-c124f39bc88c:
            targetId: a2ac3656-e561-0166-8bb5-5e600edaca26
            port: SUCCESS
          7487d5d5-e6ed-5d2e-8b7d-83ce966f2983:
            targetId: a2ac3656-e561-0166-8bb5-5e600edaca26
            port: WARNING
    results:
      SUCCESS:
        a2ac3656-e561-0166-8bb5-5e600edaca26:
          x: 480
          'y': 174
