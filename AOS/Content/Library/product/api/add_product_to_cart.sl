namespace: product.api
flow:
  name: add_product_to_cart
  inputs:
    - url: "${get_sp('url')}"
    - username: thanyathorn
    - password:
        default: P@ssw0rd
        sensitive: true
    - product_id: '1'
    - color_code: C3C3C3
  workflow:
    - authenticate:
        do:
          io.cloudslang.base.http.http_client_post:
            - url: "${url+'/accountservice/ws/AccountLoginRequest'}"
            - body: "${'<?xml version=\"1.0\" encoding=\"UTF-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body><AccountLoginRequest xmlns=\"com.advantage.online.store.accountservice\"><email></email><loginPassword>%s</loginPassword><loginUser>%s</loginUser></AccountLoginRequest></soap:Body></soap:Envelope>' % (password, username)}"
            - content_type: text/xml
        publish:
          - soap: '${return_result}'
        navigate:
          - SUCCESS: get_user_id
          - FAILURE: on_failure
    - get_user_id:
        do:
          io.cloudslang.base.xml.xpath_query:
            - xml_document: '${soap}'
            - xpath_query: '//ns2:userId/text()'
        publish:
          - user_id: '${selected_value}'
        navigate:
          - SUCCESS: get_auth_token
          - FAILURE: on_failure
    - get_auth_token:
        do:
          io.cloudslang.base.xml.xpath_query:
            - xml_document: '${soap}'
            - xpath_query: '//ns2:t_authorization/text()'
        publish:
          - token: '${selected_value}'
        navigate:
          - SUCCESS: add_product_to_cart
          - FAILURE: on_failure
    - add_product_to_cart:
        do:
          io.cloudslang.base.http.http_client_post:
            - url: "${'%s/order/api/v1/carts/%s/product/%s/color/%s' % (url, user_id, product_id, color_code)}"
            - auth_type: anonymous
            - headers: '${"Authorization: Basic "+token}'
        publish:
          - cart_json: '${return_result}'
        navigate:
          - SUCCESS: SUCCESS
          - FAILURE: on_failure
  outputs:
    - cart_json: '${cart_json}'
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      authenticate:
        x: 92
        'y': 153
      get_user_id:
        x: 92
        'y': 344
      get_auth_token:
        x: 273
        'y': 349
      add_product_to_cart:
        x: 468
        'y': 344
        navigate:
          1ff41421-b70e-26a6-84cf-75f0eaaef6ab:
            targetId: 700f5492-20a6-4bc4-e6fd-394c8b520e31
            port: SUCCESS
    results:
      SUCCESS:
        700f5492-20a6-4bc4-e6fd-394c8b520e31:
          x: 272
          'y': 164
