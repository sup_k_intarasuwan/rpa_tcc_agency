namespace: product.api.sub_flow
flow:
  name: test
  workflow:
    - Add_Excel_Data:
        do_external:
          4552e495-4595-4916-b58b-ce521bdb1e9a:
            - excelFileName: '${file_path}'
            - worksheetName: product
            - headerData: "${'Category ID,Category Name,Product ID,Product Name,Product Price,'+','.join(['Color Code'] * 8)}"
            - rowData: "${category_id+','+category_name+','+product_id+','+product_name+','+product_price+','+color_codes}"
            - columnDelimiter: ','
            - rowsDelimiter: '|'
            - overwriteData: 'false'
        navigate:
          - failure: on_failure
    - is_excel:
        do:
          io.cloudslang.base.utils.is_true:
            - bool_value: '${str(file_path.endswith("xls") or file_path.endswith("xlsx"))}'
        navigate: []
    - delete:
        do:
          io.cloudslang.base.filesystem.delete:
            - source: '${file_path}'
        navigate:
          - FAILURE: on_failure
    - New_Excel_Document:
        do_external:
          9d21ca68-7d03-4fb3-9478-03956532bf6f:
            - excelFileName: '${file_path}'
            - worksheetNames: product
            - delimiter: ','
        navigate:
          - failure: on_failure
    - add_product_to_cart:
        do:
          product.api.add_product_to_cart:
            - product_id: "${data.split('|')[2].split(',')[product_id_index]}"
            - color_code: "${data.split('|')[2].split(',')[color_code_index]}"
        navigate:
          - FAILURE: on_failure
  results:
    - FAILURE
extensions:
  graph:
    steps:
      Add_Excel_Data:
        x: 334
        'y': 47
      is_excel:
        x: 189
        'y': 84
      delete:
        x: 291
        'y': 255
      New_Excel_Document:
        x: 519
        'y': 206
      add_product_to_cart:
        x: 402
        'y': 438
