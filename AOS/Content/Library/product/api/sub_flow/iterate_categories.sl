namespace: product.api.sub_flow
flow:
  name: iterate_categories
  inputs:
    - json
    - file_path
    - category_id
  workflow:
    - get_category:
        do:
          io.cloudslang.base.json.json_path_query:
            - json_object: '${json}'
            - json_path: '${"$[?(@.categoryId == "+category_id+")]"}'
        publish:
          - category_json: '${return_result}'
        navigate:
          - SUCCESS: get_category_name
          - FAILURE: on_failure
    - get_category_name:
        do:
          io.cloudslang.base.json.json_path_query:
            - json_object: '${category_json}'
            - json_path: '$.*.categoryName'
        publish:
          - category_name: '${return_result[2:-2]}'
        navigate:
          - SUCCESS: get_product_ids
          - FAILURE: on_failure
    - get_product_ids:
        do:
          io.cloudslang.base.json.json_path_query:
            - json_object: '${category_json}'
            - json_path: '$.*.products.*.productId'
        publish:
          - product_ids: '${return_result[1:-1]}'
        navigate:
          - SUCCESS: iterate_products
          - FAILURE: on_failure
    - iterate_products:
        loop:
          for: product_id in product_ids
          do:
            product.api.sub_flow.iterate_products:
              - json: '${category_json}'
              - category_name: '${category_name}'
              - category_id: '${category_id}'
              - product_id: '${product_id}'
              - file_path: '${file_path}'
          break:
            - FAILURE
        navigate:
          - FAILURE: on_failure
          - SUCCESS: SUCCESS
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      get_category_name:
        x: 300
        'y': 107
      get_category:
        x: 86
        'y': 108
      get_product_ids:
        x: 516
        'y': 105
      iterate_products:
        x: 517
        'y': 306
        navigate:
          942d9b9e-9045-5454-0cf9-88853158e589:
            targetId: 3a65f595-7ff8-ee97-6e63-b9b51545156a
            port: SUCCESS
    results:
      SUCCESS:
        3a65f595-7ff8-ee97-6e63-b9b51545156a:
          x: 706
          'y': 103
