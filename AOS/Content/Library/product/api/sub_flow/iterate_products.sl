namespace: product.api.sub_flow
flow:
  name: iterate_products
  inputs:
    - json
    - category_name
    - category_id
    - product_id
    - file_path
  workflow:
    - get_product_name:
        do:
          io.cloudslang.base.json.json_path_query:
            - json_object: '${json}'
            - json_path: '${"$.*.products[?(@.productId == " + product_id + ")].productName"}'
        publish:
          - product_name: '${return_result[2:-2]}'
        navigate:
          - SUCCESS: get_product_price
          - FAILURE: on_failure
    - get_product_price:
        do:
          io.cloudslang.base.json.json_path_query:
            - json_object: '${json}'
            - json_path: '${"$.*.products[?(@.productId == " + product_id + ")].price"}'
        publish:
          - product_price: '${return_result[1:-1]}'
        navigate:
          - SUCCESS: get_color_codes
          - FAILURE: on_failure
    - get_color_codes:
        do:
          io.cloudslang.base.json.json_path_query:
            - json_object: '${json}'
            - json_path: '${"$.*.products[?(@.productId == " + product_id + ")].colors.*.code"}'
        publish:
          - color_codes: "${return_result.replace('\"','')[1:-1]}"
        navigate:
          - SUCCESS: is_excel
          - FAILURE: on_failure
    - add_product:
        do:
          io.cloudslang.base.filesystem.add_text_to_file:
            - file_path: '${file_path}'
            - text: "${\"|\"+\"|\".join([category_id.rjust(13),category_name.ljust(15),product_id.rjust(12),product_name.ljust(51),product_price.rjust(15),color_codes.ljust(60)])+\"|\\n\"}"
        navigate:
          - SUCCESS: SUCCESS
          - FAILURE: on_failure
    - is_excel:
        do:
          io.cloudslang.base.utils.is_true:
            - bool_value: '${str(file_path.endswith("xls") or file_path.endswith("xlsx"))}'
        navigate:
          - 'TRUE': Add_Excel_Data
          - 'FALSE': add_product
    - Add_Excel_Data:
        do_external:
          4552e495-4595-4916-b58b-ce521bdb1e9a:
            - excelFileName: '${file_path}'
            - worksheetName: product
            - headerData: "${'Category ID,Category Name,Product ID,Product Name,Product Price,'+','.join(['Color Code%s' % n for n in range(1, 9)])}"
            - rowData: "${category_id+','+category_name+','+product_id+','+product_name+','+product_price+','+color_codes}"
            - columnDelimiter: ','
            - rowsDelimiter: '|'
            - overwriteData: 'false'
        navigate:
          - failure: on_failure
          - success: SUCCESS
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      get_product_name:
        x: 93
        'y': 105
      get_product_price:
        x: 104
        'y': 299
      get_color_codes:
        x: 302
        'y': 299
      add_product:
        x: 471
        'y': 200
        navigate:
          0c06074d-18d7-61ab-85aa-ae55acb6cb7b:
            targetId: de6ff808-ef33-d19c-1bd8-7d96c672f7cf
            port: SUCCESS
      is_excel:
        x: 300
        'y': 135
      Add_Excel_Data:
        x: 466
        'y': 45
        navigate:
          10abe7c3-7ee4-47b2-d902-9cb3d8cfbc8c:
            targetId: de6ff808-ef33-d19c-1bd8-7d96c672f7cf
            port: success
    results:
      SUCCESS:
        de6ff808-ef33-d19c-1bd8-7d96c672f7cf:
          x: 637
          'y': 111
