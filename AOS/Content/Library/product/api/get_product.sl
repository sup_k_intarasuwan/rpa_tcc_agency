namespace: product.api
flow:
  name: get_product
  inputs:
    - aos_url: "${get_sp('url')}"
    - file_path: "${get_sp('excel_path')}"
  workflow:
    - http_client_get:
        do:
          io.cloudslang.base.http.http_client_get:
            - url: '${aos_url + "catalog/api/v1/categories/all_data"}'
            - proxy_port: '8080'
        publish:
          - json: '${return_result}'
        navigate:
          - SUCCESS: is_excel
          - FAILURE: on_failure
    - write_to_file:
        do:
          io.cloudslang.base.filesystem.write_to_file:
            - file_path: '${file_path}'
            - text: "${'+'+'+'.join([''.center(13,'-'),''.center(15,'-'),''.center(12,'-'),''.center(51,'-'),''.center(15,'-'),''.center(60,'-')])+'+\\n'+\\\n'|'+'|'.join(['Category ID'.center(13),'Category Name'.center(15),'Product ID'.center(12),'Product Name'.center(51),'Product Price'.center(15),'Color Codes'.center(60)])+'|\\n'+\\\n'+'+'+'.join([''.center(13,'-'),''.center(15,'-'),''.center(12,'-'),''.center(51,'-'),''.center(15,'-'),''.center(60,'-')])+'+\\n'}"
        navigate:
          - SUCCESS: get_categories
          - FAILURE: on_failure
    - get_categories:
        do:
          io.cloudslang.base.json.json_path_query:
            - json_object: '${json}'
            - json_path: '$.*.categoryId'
        publish:
          - category_id_list: '${return_result[1:-1]}'
        navigate:
          - SUCCESS: iterate_categories
          - FAILURE: on_failure
    - iterate_categories:
        loop:
          for: category_id in category_id_list
          do:
            product.api.sub_flow.iterate_categories:
              - json: '${json}'
              - file_path: '${file_path}'
              - category_id: '${category_id}'
          break:
            - FAILURE
        navigate:
          - FAILURE: on_failure
          - SUCCESS: SUCCESS
    - is_excel:
        do:
          io.cloudslang.base.utils.is_true:
            - bool_value: '${str(file_path.endswith("xls") or file_path.endswith("xlsx"))}'
        navigate:
          - 'TRUE': delete
          - 'FALSE': write_to_file
    - delete:
        do:
          io.cloudslang.base.filesystem.delete:
            - source: '${file_path}'
        navigate:
          - SUCCESS: New_Excel_Document
          - FAILURE: on_failure
    - New_Excel_Document:
        do_external:
          9d21ca68-7d03-4fb3-9478-03956532bf6f:
            - excelFileName: '${file_path}'
            - worksheetNames: product
            - delimiter: ','
        navigate:
          - failure: on_failure
          - success: get_categories
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      http_client_get:
        x: 99
        'y': 251
      write_to_file:
        x: 430
        'y': 342
      get_categories:
        x: 629
        'y': 343
      iterate_categories:
        x: 811
        'y': 342
        navigate:
          b1d096a3-d04e-1af0-57e2-ac1a97078c7e:
            targetId: e55b3e47-ad3b-f642-e879-4531eb85600d
            port: SUCCESS
      is_excel:
        x: 260
        'y': 248
      delete:
        x: 439
        'y': 169
      New_Excel_Document:
        x: 625
        'y': 170
    results:
      SUCCESS:
        e55b3e47-ad3b-f642-e879-4531eb85600d:
          x: 803
          'y': 170
