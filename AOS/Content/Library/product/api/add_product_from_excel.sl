namespace: product.api
flow:
  name: add_product_from_excel
  inputs:
    - excel_path: "${get_sp('excel_path')}"
    - sheet: product
    - product_id_header: Product ID
    - color_code_header: Color Code1
  workflow:
    - Get_Cell:
        do_external:
          5060d8cc-d03c-43fe-946f-7babaaec589f:
            - excelFileName: '${excel_path}'
            - worksheetName: product
            - hasHeader: 'yes'
            - firstRowIndex: '0'
            - rowIndex: '0:1000'
            - columnIndex: '0:100'
            - rowDelimiter: '|'
            - columnDelimiter: ','
            - product_id_header: '${product_id_header}'
            - color_code_header: '${color_code_header}'
        publish:
          - header
          - data: '${returnResult}'
          - product_id_index: '${str(header.split(",").index(product_id_header))}'
          - color_code_index: '${str(header.split(",").index(color_code_header))}'
        navigate:
          - failure: on_failure
          - success: add_product_to_cart
    - add_product_to_cart:
        do:
          product.api.add_product_to_cart:
            - product_id: "${data.split('|')[2].split(',')[int(product_id_index)]}"
            - color_code: "${data.split('|')[2].split(',')[int(color_code_index)]}"
        navigate:
          - FAILURE: on_failure
          - SUCCESS: SUCCESS
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      Get_Cell:
        x: 103
        'y': 150
      add_product_to_cart:
        x: 193
        'y': 278
        navigate:
          1a5ebc93-8365-36df-c76f-4f609c1e4f02:
            targetId: 53d09923-ade3-b8fc-9c55-fffd116719d5
            port: SUCCESS
    results:
      SUCCESS:
        53d09923-ade3-b8fc-9c55-fffd116719d5:
          x: 287
          'y': 146
