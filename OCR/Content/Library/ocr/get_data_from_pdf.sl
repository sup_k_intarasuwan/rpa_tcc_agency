namespace: ocr
flow:
  name: get_data_from_pdf
  inputs:
    - pdf_path: "c:\\\\temp\\\\product.pdf"
  workflow:
    - extract_text_from_pdf:
        do:
          io.cloudslang.tesseract.ocr.extract_text_from_pdf:
            - file_path: '${pdf_path}'
            - data_path: "c:\\\\temp"
            - language: ENG
            - page_index: '1'
        publish:
          - return_result
          - row_string: '${text_string}'
          - text_string: "${row_string.split('Category ID Category Name ProductID Product Name')[1][1:]}"
        navigate:
          - SUCCESS: SUCCESS
          - FAILURE: on_failure
  outputs:
    - row_string: '${text_string}'
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      extract_text_from_pdf:
        x: 215
        'y': 95
        navigate:
          b128ac47-d3d3-bc25-67c4-2cd6446473e3:
            targetId: cf710209-54a7-b357-665f-87acb4abb839
            port: SUCCESS
    results:
      SUCCESS:
        cf710209-54a7-b357-665f-87acb4abb839:
          x: 405
          'y': 93
