namespace: ocr
flow:
  name: input_data_to_excel
  inputs:
    - sheet: product_pdf
    - excel_path: "C:\\\\temp\\\\product2.xls"
    - pdf_path: "C:\\\\temp\\\\product.pdf"
  workflow:
    - delete:
        do:
          io.cloudslang.base.filesystem.delete:
            - source: '${excel_path}'
        navigate:
          - SUCCESS: New_Excel_Document
          - FAILURE: on_failure
    - New_Excel_Document:
        do_external:
          9d21ca68-7d03-4fb3-9478-03956532bf6f:
            - excelFileName: '${excel_path}'
            - worksheetNames: '${sheet}'
            - delimiter: ','
        navigate:
          - failure: on_failure
          - success: get_data_from_pdf
    - Add_Excel_Data:
        loop:
          do_external:
            4552e495-4595-4916-b58b-ce521bdb1e9a:
              - excelFileName: '${excel_path}'
              - worksheetName: '${sheet}'
              - headerData: 'Category ID,Category Name,Product ID,Product Name'
              - rowData: "${row.split()[0]+','+row.split()[1]+','+row.split()[2]+','+' '.join([row.split()[i] for i in range(3,len(row.split()))])}"
              - columnDelimiter: ','
              - rowsDelimiter: '|'
              - overwriteData: 'false'
          for: row in row_string.splitlines()
          break: []
        navigate:
          - failure: on_failure
          - success: SUCCESS
    - get_data_from_pdf:
        do:
          ocr.get_data_from_pdf:
            - pdf_path: '${pdf_path}'
        publish:
          - row_string
        navigate:
          - FAILURE: on_failure
          - SUCCESS: Add_Excel_Data
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      New_Excel_Document:
        x: 102
        'y': 168
      Add_Excel_Data:
        x: 442
        'y': 170
        navigate:
          125f3534-d11e-55e1-f617-f9ad4ef3c65a:
            targetId: f4cdcb64-32a3-b4e0-1691-9f0b81b2f941
            port: success
      get_data_from_pdf:
        x: 275
        'y': 175
      delete:
        x: 100
        'y': 341
    results:
      SUCCESS:
        f4cdcb64-32a3-b4e0-1691-9f0b81b2f941:
          x: 603
          'y': 165
