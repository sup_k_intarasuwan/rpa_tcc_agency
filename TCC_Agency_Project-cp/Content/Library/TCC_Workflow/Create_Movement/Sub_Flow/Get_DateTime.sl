namespace: TCC_Workflow.Create_Movement.Sub_Flow
flow:
  name: Get_DateTime
  inputs:
    - Container_Number: TWCU2057814
    - Data_Date
    - Idx_Date
  workflow:
    - Get_Date_o:
        do:
          TCC_Workflow.Create_Movement.Sub_Flow.Operation.Get_Date_o:
            - Data_Date: '${Date_Data}'
            - Container_Number: '${Container_Number}'
            - Idx_Date: '${Idx_DateTime}'
        publish:
          - Value_DateTime
        navigate:
          - SUCCESS: SUCCESS
  outputs:
    - Value_DateTime: '${Value_DateTime}'
  results:
    - SUCCESS
extensions:
  graph:
    steps:
      Get_Date_o:
        x: 154
        'y': 223
        navigate:
          871556d0-cbec-49fa-f9fe-ed5f07dc23fe:
            targetId: ddfbd95d-c690-5334-683d-135537c0cc1c
            port: SUCCESS
    results:
      SUCCESS:
        ddfbd95d-c690-5334-683d-135537c0cc1c:
          x: 395
          'y': 225
