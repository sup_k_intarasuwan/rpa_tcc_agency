namespace: TCC_Workflow.Create_Movement.Sub_Flow.Operation
flow:
  name: Get_Date
  inputs:
    - Date_File_Path: "\\\\tccsrv4.tccs.co.th\\tcc_file\\Movement\\Import_Full_Inventory_CSV2021_08_05_060157.csv"
    - Date_File_Sheet_Name: Import_Full_Inventory_CSV2021_0
    - Container_Number
  workflow:
    - Get_Cell:
        do_external:
          5060d8cc-d03c-43fe-946f-7babaaec589f:
            - excelFileName: '${Date_File_Path}'
            - worksheetName: '${Date_File_Sheet_Name}'
            - hasHeader: 'yes'
            - rowDelimiter: '|'
            - columnDelimiter: ','
        publish:
          - Date: '${returnResult}'
        navigate:
          - failure: on_failure
          - success: SUCCESS
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      Get_Cell:
        x: 150
        'y': 168
        navigate:
          0d3fe0ad-5948-0ed1-9fac-4e97e46b4487:
            targetId: 8850bfb9-2947-d376-015f-0ece603b15e8
            port: success
    results:
      SUCCESS:
        8850bfb9-2947-d376-015f-0ece603b15e8:
          x: 361
          'y': 169
