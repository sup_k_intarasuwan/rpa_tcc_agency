namespace: TCC_Workflow.Create_Movement.Sub_Flow.Operation
operation:
  name: Select_Container_List_Data
  inputs:
    - Data
  python_action:
    use_jython: false
    script: "def execute(Data):\n\n    Lcontainer_List = Data.split('|')\n    del Lcontainer_List[0]\n    New_container_List = []\n    #Lcontainer_List = ALL 'Container_List_Data' cut row 0 \n    #Rcontainer_List = Row of container list\n    for Rcontainer_List in Lcontainer_List:\n        if Rcontainer_List.split(',')[0] == '':\n            break\n        else:    \t\n            New_container_List.append(Rcontainer_List)\n                \n    Container_List_Data = '|'.join(New_container_List)\n    \n    return {'Container_List_Data':Container_List_Data}"
  outputs:
    - Container_List_Data: '${Container_List_Data}'
  results:
    - SUCCESS
