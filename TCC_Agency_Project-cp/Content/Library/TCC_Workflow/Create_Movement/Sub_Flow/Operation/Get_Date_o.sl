namespace: TCC_Workflow.Create_Movement.Sub_Flow.Operation
operation:
  name: Get_Date_o
  inputs:
    - Data_Date
    - Container_Number
    - Idx_Date
  python_action:
    use_jython: false
    script: "# do not remove the execute function\ndef execute(Data_Date, Idx_Date, Container_Number):\n    \n    for row in Data_Date.split('|'):\n        if row.find(Container_Number)!=(-1):\n            Value_DateTime = row.split(',')[int(Idx_Date)]\n            break\n        else:\n            Value_DateTime = 'Test'\n        \n    return{'Value_DateTime':Value_DateTime}\n    # code goes here\n# you can add additional helper methods below."
  outputs:
    - Value_DateTime
  results:
    - SUCCESS
