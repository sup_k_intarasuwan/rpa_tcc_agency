namespace: TCC_Workflow.Create_Movement.Sub_Flow.Operation
operation:
  name: Get_Container_Number
  inputs:
    - Data_Row
    - Index
  python_action:
    use_jython: false
    script: "def execute(Data_Row, Index):\n    \n    Cntr_Number = Data_Row.split(',')[int(Index)]\n    \n    return {'Cntr_Number':Cntr_Number}"
  outputs:
    - Result: '${Cntr_Number}'
  results:
    - SUCCESS
