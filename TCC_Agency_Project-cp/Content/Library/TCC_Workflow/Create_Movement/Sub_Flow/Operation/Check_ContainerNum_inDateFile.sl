namespace: TCC_Workflow.Create_Movement.Sub_Flow.Operation
operation:
  name: Check_ContainerNum_inDateFile
  inputs:
    - Date_Data
    - Container_Number
  python_action:
    use_jython: false
    script: "def execute(Date_Data, Container_Number):\n    \n    Select_Cntr_inDateList = [row for row in Date_Data.split('|') if row.find(Container_Number)!=(-1)]\n    if len(Select_Cntr_inDateList) == 0:\n        BL_Macth = 'False'\n    else:\n        BL_Macth = 'True'\n        Date_Row = Select_Cntr_inDateList\n    return {'Cntr_Macth':Cntr_Macth,\n            'Date_Row':Date_Row}"
  outputs:
    - Container_Macth: '${Cntr_Macth}'
    - Date_Row: '${Date_Row}'
  results:
    - SUCCESS
