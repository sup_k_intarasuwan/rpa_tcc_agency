namespace: TCC_Workflow.Create_Movement.Sub_Flow.Operation
operation:
  name: Get_Value
  inputs:
    - Data_Row
    - Index
  python_action:
    use_jython: false
    script: "def execute(Data_Row, Index):\n    \n    Value = Data_Row.split(',')[int(Index)]\n    \n    return {'Value':Value}"
  outputs:
    - Value: '${Value}'
  results:
    - SUCCESS
