namespace: TCC_Workflow.Create_Movement.Sub_Flow.Operation
operation:
  name: Sort_Value
  inputs:
    - Value_Container_Number
    - Value_Container_Size
    - Value_BL_Number
    - Value_Status
    - Value_Voyage
    - Value_Port_Code
    - Value_Vessel
    - Value_DateTime
  python_action:
    use_jython: false
    script: "# do not remove the execute function\ndef execute(Value_Container_Number,\n            Value_Container_Size, \n            Value_BL_Number, \n            Value_Status, \n            Value_Voyage, \n            Value_Port_Code, \n            Value_Vessel, \n            Value_DateTime):\n                \n    if Value_Status == 'empty':\n        Value_Dynamic_Code = 'DCHE'\n        Value_Status = 'E'\n    else:\n        Value_Dynamic_Code = 'DCHF'\n        Value_Status = 'F'\n            \n    Row_Result_List = [Value_Container_Number, Value_Container_Size, Value_Dynamic_Code, Value_DateTime, Value_Port_Code, 'PAT', Value_Status, Value_BL_Number, Value_Vessel, Value_Voyage]  \n    Row_Result = ','.join(Row_Result_List)\n        \n    return {'Row_Result':Row_Result}"
  outputs:
    - Row_Result: '${Row_Result}'
  results:
    - SUCCESS
