namespace: TCC_Workflow.Create_Movement.Sub_Flow
flow:
  name: Get_Container_Detail
  inputs:
    - Container_List_Path: "\\\\tccsrv4.tccs.co.th\\tcc_file\\Movement\\CONTAINER LIST  OF INCRES V. 2129S _ LCH.xls"
    - Container_List_SheetName: LCH
  workflow:
    - get_vessel_detail:
        do:
          io.cloudslang.base.excel.get_cell:
            - excel_file_name: '${Container_List_Path}'
            - worksheet_name: '${Container_List_SheetName}'
            - has_header: 'no'
            - first_row_index: '0'
            - row_index: '0:5'
            - row_delimiter: '|'
            - column_delimiter: ','
        publish:
          - Data: '${return_result}'
          - Value_Voyage: "${str([x.split(',')[2].split(' ')[4] for x in Data.split('|') if x.split(',')[1] == 'VESSEL'])[2:-2]}"
          - Value_Vessel: "${str([x.split(',')[2].split(' ')[0] for x in Data.split('|') if x.split(',')[1] == 'VESSEL'])[2:-2]}"
        navigate:
          - SUCCESS: get_cntr_list_detail
          - FAILURE: on_failure
    - get_cntr_list_detail:
        do:
          io.cloudslang.base.excel.get_cell:
            - excel_file_name: '${Container_List_Path}'
            - worksheet_name: '${Container_List_SheetName}'
            - has_header: 'yes'
            - first_row_index: '6'
            - row_delimiter: '|'
            - column_delimiter: ','
        publish:
          - Data: '${return_result}'
          - header
          - Idx_Container_Number: "${str(header.split(\",\").index('Container No.'))}"
          - Idx_Container_Size: "${str(header.split(\",\").index('Ctnr Size'))}"
          - Idx_BL_Number: "${str(header.split(\",\").index('BL Number'))}"
          - Idx_Status: "${str(header.split(\",\").index('Status'))}"
          - Idx_Port_Code: "${str(header.split(\",\").index('POD'))}"
        navigate:
          - SUCCESS: Select_Container_List_Data_1
          - FAILURE: on_failure
    - Select_Container_List_Data_1:
        do:
          TCC_Workflow.Create_Movement.Sub_Flow.Operation.Select_Container_List_Data:
            - Data: '${Data}'
        publish:
          - Container_List_Data
        navigate:
          - SUCCESS: SUCCESS
  outputs:
    - Container_List_Data: '${Container_List_Data}'
    - Idx_Container_Size: '${Idx_Container_Size}'
    - Idx_Container_Number: '${Idx_Container_Number}'
    - Idx_BL_Number: '${Idx_BL_Number}'
    - Idx_Status: '${Idx_Status}'
    - Idx_Port_Code: '${Idx_Port_Code}'
    - Value_Voyage: '${Value_Voyage}'
    - Value_Vessel: '${Value_Vessel}'
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      get_vessel_detail:
        x: 86
        'y': 321
      get_cntr_list_detail:
        x: 86
        'y': 149
      Select_Container_List_Data_1:
        x: 267
        'y': 151
        navigate:
          c80ff8c0-c885-e4b6-6cc9-f9f0bc744ab3:
            targetId: 46cabecf-9740-a66a-c6a5-c50ac163a897
            port: SUCCESS
    results:
      SUCCESS:
        46cabecf-9740-a66a-c6a5-c50ac163a897:
          x: 476
          'y': 149
