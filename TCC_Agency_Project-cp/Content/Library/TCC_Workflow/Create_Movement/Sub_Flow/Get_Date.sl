namespace: TCC_Workflow.Create_Movement.Sub_Flow
flow:
  name: Get_Date
  inputs:
    - Date_File_Path: "\\\\192.168.130.136\\c$\\tcc_file\\Movement\\CONTAINER LIST  OF INCRES V. 2129S _ LCH.xls"
    - Date_File_Sheet_Name: LCH
    - Container_Number: TWCU8042581
  workflow:
    - Select_Excel_Row:
        do_external:
          dddf8f86-f9be-4bbf-8501-282a335b50df:
            - excelFileName: '${Date_File_Path}'
            - worksheetName: '${Date_File_Sheet_Name}'
            - hasHeader: 'yes'
            - value: '${Container_Number}'
            - columnIndex: '0'
        publish:
          - header
          - rowsCount
          - returnResult
        navigate:
          - failure: on_failure
          - success: SUCCESS
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      Select_Excel_Row:
        x: 268
        'y': 214
        navigate:
          32978f2b-f938-369a-7a0e-32b3dffc7def:
            targetId: 8850bfb9-2947-d376-015f-0ece603b15e8
            port: success
    results:
      SUCCESS:
        8850bfb9-2947-d376-015f-0ece603b15e8:
          x: 474
          'y': 218
