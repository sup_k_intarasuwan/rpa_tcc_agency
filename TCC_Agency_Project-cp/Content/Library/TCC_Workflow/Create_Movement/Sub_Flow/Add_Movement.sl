namespace: TCC_Workflow.Create_Movement.Sub_Flow
flow:
  name: Add_Movement
  inputs:
    - Movement_List_Path: "\\\\192.168.130.136\\c$\\tcc_file\\Movement\\MOVEMENT LIST 2021-1.xlsx"
    - Movement_List_Year_SheetName: 'WHOLE LIST OF THBKK&THLCH-2021'
    - container_list_data_source: "\\\\192.168.130.136\\c$\\tcc_file\\Movement\\CONTAINER LIST  OF INCRES V. 2129S _ LCH.xls"
    - container_list_data_sheetname: LCH
    - date_data_source: "\\\\192.168.130.136\\c$\\TCC_File\\Movement\\Import_Full_Inventory_CSV2021_08_05_060157.csv"
  workflow:
    - Get_Container_Detail:
        do:
          TCC_Workflow.Create_Movement.Sub_Flow.Get_Container_Detail:
            - Container_List_Path: '${container_list_data_source}'
            - Container_List_SheetName: '${container_list_data_sheetname}'
        publish:
          - Container_List_Data
          - Idx_Container_Size
          - Idx_Container_Number
          - Idx_BL_Number
          - Idx_Status
          - Idx_Port_Code
          - Value_Voyage
          - Value_Vessel
        navigate:
          - FAILURE: on_failure
          - SUCCESS: read_from_file
    - list_iterator:
        do:
          io.cloudslang.base.lists.list_iterator:
            - list: '${Container_List_Data}'
            - separator: '|'
        publish:
          - Container_List_Row: '${result_string}'
        navigate:
          - HAS_MORE: Get_Container_Number
          - NO_MORE: SUCCESS
          - FAILURE: on_failure
    - Sort_Value_1:
        do:
          TCC_Workflow.Create_Movement.Sub_Flow.Operation.Sort_Value:
            - Value_Container_Number: '${Value_Container_Number}'
            - Value_Container_Size: "${Container_List_Row.split(',')[int(Idx_Container_Size)]}"
            - Value_BL_Number: "${Container_List_Row.split(',')[int(Idx_BL_Number)]}"
            - Value_Status: "${Container_List_Row.split(',')[int(Idx_Status)]}"
            - Value_Voyage: '${Value_Voyage}'
            - Value_Port_Code: "${Container_List_Row.split(',')[int(Idx_Port_Code)]}"
            - Value_Vessel: '${Value_Vessel}'
            - Value_DateTime: '${Value_DateTime}'
        publish:
          - Container_List_Row: '${Row_Result}'
        navigate:
          - SUCCESS: Add_Excel_Data
    - Add_Excel_Data:
        do_external:
          4552e495-4595-4916-b58b-ce521bdb1e9a:
            - excelFileName: '${Movement_List_Path}'
            - worksheetName: '${Movement_List_Year_SheetName}'
            - rowData: '${Row_Result}'
        navigate:
          - failure: on_failure
          - success: list_iterator
    - read_from_file:
        do:
          io.cloudslang.base.filesystem.read_from_file:
            - file_path: '${date_data_source}'
        publish:
          - header: '${read_text[3:-3].splitlines()[0]}'
          - data: "${'|'.join(read_text[3:-4].splitlines()[1:len(read_text[3:-4].splitlines())])}"
          - Idx_DateTime: "${str(header.split(\",\").index('Intime'))}"
          - Date_Data: "${'|'.join([c.replace(c.split('\"')[1],c.split('\"')[1].replace(',','%')) if c.find('\"') != -1 else c for c in data.split('|')])}"
        navigate:
          - SUCCESS: list_iterator
          - FAILURE: on_failure
    - Get_Container_Number:
        do:
          TCC_Workflow.Create_Movement.Sub_Flow.Operation.Get_Container_Number:
            - Data_Row: '${Container_List_Row}'
            - Index: '${Idx_Container_Number}'
        publish:
          - Value_Container_Number: '${Result}'
        navigate:
          - SUCCESS: Get_Date_o
    - Get_Date_o:
        do:
          TCC_Workflow.Create_Movement.Sub_Flow.Operation.Get_Date_o:
            - Data_Date: '${Date_Data}'
            - Container_Number: '${Value_Container_Number}'
            - Idx_Date: '${Idx_DateTime}'
        publish:
          - Value_DateTime
        navigate:
          - SUCCESS: Sort_Value_1
  results:
    - SUCCESS
    - FAILURE
extensions:
  graph:
    steps:
      Sort_Value_1:
        x: 618
        'y': 297
      Add_Excel_Data:
        x: 405
        'y': 172
      Get_Date_o:
        x: 517
        'y': 470
      Get_Container_Detail:
        x: 71
        'y': 158
      list_iterator:
        x: 249
        'y': 335
        navigate:
          c26f70a5-7e12-0912-87de-168fc47e8f40:
            targetId: dcfcd4a2-72ef-b980-4693-0c0048ab3ada
            port: NO_MORE
      read_from_file:
        x: 72
        'y': 348
      Get_Container_Number:
        x: 334
        'y': 490
    results:
      SUCCESS:
        dcfcd4a2-72ef-b980-4693-0c0048ab3ada:
          x: 250
          'y': 121
