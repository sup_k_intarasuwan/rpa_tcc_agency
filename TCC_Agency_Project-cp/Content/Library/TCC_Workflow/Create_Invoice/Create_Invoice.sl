########################################################################################################################
#!!
#! @description: สร้าง Invoice จาก Excel data source โดย compare ระหว่าง jobcontainerlist ที่ Export ใน Format Excel มาจากระบบ
#!               แล้วทำการนำข้อมูลเหล่านั้นมาทำการ Create Invoice ในระบ ForwardPro.
#!
#! @input excel_data_source: Source excel file for create invoice.
#! @input excel_data_sheetname: Sheet Data for create invoice.
#! @input excel_data_chargeTable_sheetname: Sheet IMP charge for create invoice.
#! @input container_list_data_source: Source excel file of Job Container List.
#! @input container_list_data_sheetname: Sheet name of Job Container List.
#! @input company_header: Company for header in ForwardPro.
#! @input forwardpro_user: ForwardPro user.
#! @input forwardpro_password: ForwardPro password.
#! @input sea_freight_menu: Select menu in Sea Freight.
#!!#
########################################################################################################################
namespace: TCC_Workflow.Create_Invoice
flow:
  name: Create_Invoice
  inputs:
    - excel_data_source: "\\\\tccsrv4.tccs.co.th\\tcc_file\\SAMPLE file for IMP charge.xls"
    - excel_data_sheetname: ADD
    - excel_data_chargeTable_sheetname: IMP charge
    - container_list_data_source: "\\\\tccsrv4.tccs.co.th\\tcc_file\\sfjobctnrlist.xls"
    - container_list_data_sheetname: Sheet1
    - company_header: 'TCC AGENCY CO., LTD.'
    - forwardpro_user: RPA01
    - forwardpro_password:
        default: '1234'
        sensitive: true
    - sea_freight_menu: S/F Inbound Transaction List
    - telex_data_source: "\\\\tccsrv4.tccs.co.th\\tcc_file\\G. BOX 2128S TLX.XLSX"
    - telex_data_sheetname: sheet
    - freetime1_data_source: "\\\\tccsrv4.tccs.co.th\\tcc_file\\G. BOX 2128S -TH.xlsx"
    - freetime1_data_sheetname: Sheet1
    - freetime2_data_source: "\\\\tccsrv4.tccs.co.th\\tcc_file\\G. BOX 2128S ADD.xlsx"
    - freetime2_data_sheetname: sheet
    - default_freetime_dem: '5'
    - default_freetime_det: '3'
  workflow:
    - get_data_source_1:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.get_data_source:
            - excel_data_source: '${details_data_source}'
            - excel_data_sheetname: '${excel_data_sheetname}'
        publish:
          - details_data_source
          - header_data_source
          - Idx_First_Row
          - Idx_BL_Number
          - Idx_RPA_Status
          - Idx_20F
          - Idx_40F
          - Idx_Service_Code_Start
          - Idx_Service_Code_End
        navigate:
          - FAILURE: on_failure
          - SUCCESS: create_folder_pdf_files_1
    - Get_BL_Number_1:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.Operation.Get_BL_Number:
            - Data_Row_IMP: '${Data_Row_IMP}'
            - Idx_BL_Number: '${Idx_BL_Number}'
            - Data_from_IMP_File: '${details_data_source}'
            - Frist_Row_Idx_IMP_File: '${Idx_First_Row}'
        publish:
          - BL_Number
          - Row_Idx_Data
          - Row_Idx_inExcel
        navigate:
          - SUCCESS: Read_Before_BL_Number
    - create_folder_pdf_files_1:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.create_folder_pdf_files: []
        publish:
          - folderName
        navigate:
          - FAILURE: on_failure
          - SUCCESS: list_iterator
    - list_iterator:
        do:
          io.cloudslang.base.lists.list_iterator:
            - list: '${details_data_source}'
            - separator: '|'
        publish:
          - Data_Row_IMP: '${result_string}'
        navigate:
          - HAS_MORE: Get_BL_Number_1
          - NO_MORE: SUCCESS
          - FAILURE: on_failure
    - Failed_container_not_found:
        do:
          TCC_Workflow.Create_Invoice.RPA_Activity.Write_Excel_Data_Flow:
            - excel_Path: '${excel_data_source}'
            - sheet_Name: '${excel_data_sheetname}'
            - row_Index: '${Row_Idx_inExcel}'
            - col_Index: '${Idx_RPA_Status}'
            - cell_data: 'Failed (Container No. not found in "jobctnrlist" file.)'
        publish: []
        navigate:
          - SUCCESS: list_iterator
          - FAILURE: on_failure
    - Passed_Created_Invoice:
        do:
          TCC_Workflow.Create_Invoice.RPA_Activity.Write_Excel_Data_Flow:
            - excel_Path: '${excel_data_source}'
            - sheet_Name: '${excel_data_sheetname}'
            - row_Index: '${Row_Idx_inExcel}'
            - col_Index: '${Idx_RPA_Status}'
            - cell_data: Passed - Invoice has been create.
        publish: []
        navigate:
          - SUCCESS: Write_BL_Number_to_file
          - FAILURE: on_failure
    - Update_qty_20f:
        do:
          TCC_Workflow.Create_Invoice.RPA_Activity.Write_Excel_Data_Flow:
            - excel_Path: '${excel_data_source}'
            - sheet_Name: '${excel_data_sheetname}'
            - row_Index: '${Row_Idx_inExcel}'
            - col_Index: '${Idx_20F}'
            - cell_data: '${Count_Cntr_Size_20}'
        navigate:
          - SUCCESS: Update_qty_40f
          - FAILURE: on_failure
    - Update_qty_40f:
        do:
          TCC_Workflow.Create_Invoice.RPA_Activity.Write_Excel_Data_Flow:
            - excel_Path: '${excel_data_source}'
            - sheet_Name: '${excel_data_sheetname}'
            - row_Index: '${Row_Idx_inExcel}'
            - col_Index: '${Idx_40F}'
            - cell_data: '${Count_Cntr_Size_40}'
        navigate:
          - SUCCESS: Fwpx_CreateInvoice_Flow
          - FAILURE: on_failure
    - Write_BL_Number_to_file:
        do:
          io.cloudslang.base.filesystem.write_to_file:
            - file_path: "\\\\tccsrv4.tccs.co.th\\tcc_file\\tmp_bl_number.txt"
            - text: '${BL_Number}'
        navigate:
          - SUCCESS: list_iterator
          - FAILURE: on_failure
    - Read_Before_BL_Number:
        do:
          io.cloudslang.base.filesystem.read_from_file:
            - file_path: "\\\\tccsrv4.tccs.co.th\\tcc_file\\tmp_bl_number.txt"
        publish:
          - Temp_BL_Number: '${read_text}'
        navigate:
          - SUCCESS: get_bl_type_1
          - FAILURE: on_failure
    - Check_2ndTime_BL_Number:
        do:
          io.cloudslang.base.utils.is_true:
            - bool_value: '${str(BL_Number == Temp_BL_Number)}'
        navigate:
          - 'TRUE': Fwpx_CreateInvoice_Flow
          - 'FALSE': Update_qty_20f
    - Fwpx_CreateInvoice_Flow:
        do:
          TCC_Workflow.Create_Invoice.RPA_Activity.Fwpx_CreateInvoice_Flow:
            - BL_number: '${BL_Number}'
            - qty_20f: '${Count_Cntr_Size_20}'
            - qty_40f: '${Count_Cntr_Size_40}'
            - Row_Value: "${','.join(Data_Row_IMP.split(',')[int(Idx_Service_Code_Start):int(Idx_Service_Code_End)+1])}"
            - count_container_20GP: '${Count_Cntr_20GP}'
            - count_container_20TK: '${Count_Cntr_20TK}'
            - count_container_40GP: '${Count_Cntr_40GP}'
            - count_container_40HC: '${Count_Cntr_40HC}'
            - Code_Header: "${','.join(header_data_source.split(',')[int(Idx_Service_Code_Start):int(Idx_Service_Code_End)+1])}"
            - IMPChargeTable_ExcelPath: '${excel_data_source}'
            - IMPChargeTable_SheetName: '${excel_data_chargeTable_sheetname}'
            - company: '${company_header}'
            - user: '${forwardpro_user}'
            - password:
                value: '${forwardpro_password}'
                sensitive: true
            - sea_freight_menu: '${sea_freight_menu}'
            - BL_type: '${BL_Type}'
            - folder_pdf_report: '${folderName}'
            - freetime_dem: '${DEM}'
            - freetime_det: '${DET}'
        navigate:
          - FAILURE: Status_Fail_Fwpx
          - SUCCESS: Passed_Created_Invoice
    - Status_Fail_Fwpx:
        do:
          TCC_Workflow.Create_Invoice.RPA_Activity.Write_Excel_Data_Flow:
            - excel_Path: '${excel_data_source}'
            - sheet_Name: '${excel_data_sheetname}'
            - row_Index: '${Row_Idx_inExcel}'
            - col_Index: '${Idx_RPA_Status}'
            - cell_data: Failed (Unexpected error.)
        navigate:
          - SUCCESS: list_iterator
          - FAILURE: on_failure
    - get_bl_type_1:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.get_bl_type:
            - Telex_Path: '${telex_data_source}'
            - BL_Number: '${BL_Number}'
            - Telex_SheetName: '${telex_data_sheetname}'
        publish:
          - BL_Type
        navigate:
          - FAILURE: on_failure
          - SUCCESS: get_free_time_1
    - get_free_time_1:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.get_free_time:
            - FreeTime_File1_Path: '${freetime1_data_source}'
            - FreeTime_File1_SheetName: '${freetime1_data_sheetname}'
            - FreeTime_File2_Path: '${freetime2_data_source}'
            - FreeTime_File2_SheetName: '${freetime2_data_sheetname}'
            - BL_Number: '${BL_Number}'
            - Default_DEM: '${default_freetime_dem}'
            - Default_DET: '${default_freetime_det}'
        publish:
          - DEM
          - DET
        navigate:
          - FAILURE: on_failure
          - SUCCESS: get_container_detail
    - get_container_detail:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.get_container_detail:
            - Cnrt_List_Path: '${container_list_data_source}'
            - BL_Number: '${BL_Number}'
            - Qty_20F: "${'0' if Data_Row_IMP.split(',')[int(Idx_20F)] == '' else Data_Row_IMP.split(',')[int(Idx_20F)].split('.')[0]}"
            - Qty_40F: "${'0' if Data_Row_IMP.split(',')[int(Idx_40F)] == '' else Data_Row_IMP.split(',')[int(Idx_40F)].split('.')[0]}"
            - Cntr_List_SheetName: '${container_list_data_sheetname}'
        navigate:
          - SUCCESS: Check_2ndTime_BL_Number
          - FAILURE: Failed_container_not_found
  results:
    - SUCCESS
    - FAILURE
extensions:
  graph:
    steps:
      Fwpx_CreateInvoice_Flow:
        x: 794
        'y': 305
      Get_BL_Number_1:
        x: 316
        'y': 453
      Passed_Created_Invoice:
        x: 656
        'y': 297
      Read_Before_BL_Number:
        x: 312
        'y': 596
      get_free_time_1:
        x: 618
        'y': 606
      Update_qty_20f:
        x: 1003
        'y': 476
      Update_qty_40f:
        x: 1004
        'y': 300
      Status_Fail_Fwpx:
        x: 526
        'y': 73
      create_folder_pdf_files_1:
        x: 160
        'y': 281
      list_iterator:
        x: 321
        'y': 287
        navigate:
          e8f8aea3-55ef-3c1b-89f2-2cc8183ddb69:
            targetId: f274e46a-8c15-db4b-97dc-c62a32227cf3
            port: NO_MORE
      Failed_container_not_found:
        x: 453
        'y': 465
      get_bl_type_1:
        x: 470
        'y': 599
      get_container_detail:
        x: 611
        'y': 467
      get_data_source_1:
        x: 7
        'y': 282
      Check_2ndTime_BL_Number:
        x: 789
        'y': 482
      Write_BL_Number_to_file:
        x: 515
        'y': 290
    results:
      SUCCESS:
        f274e46a-8c15-db4b-97dc-c62a32227cf3:
          x: 149
          'y': 107
