namespace: TCC_Workflow.Create_Invoice.RPA_Activity
flow:
  name: Fwpx_CreateInvoice_Flow
  inputs:
    - BL_number
    - qty_20f
    - qty_40f
    - Row_Value
    - count_container_20GP
    - count_container_20TK
    - count_container_40GP
    - count_container_40HC
    - Code_Header
    - IMPChargeTable_ExcelPath
    - IMPChargeTable_SheetName
    - company
    - user
    - password:
        sensitive: true
    - sea_freight_menu
    - BL_type
    - folder_pdf_report
    - freetime_dem
    - freetime_det
  workflow:
    - ForwardPro_CreateInvoice:
        do:
          TCC_Workflow.Create_Invoice.RPA_Activity.ForwardPro_CreateInvoice:
            - BL_number: '${BL_number}'
            - qty_20f: '${qty_20f}'
            - qty_40f: '${qty_40f}'
            - Row_Value: '${Row_Value}'
            - count_container_20GP: '${count_container_20GP}'
            - count_container_20TK: '${count_container_20TK}'
            - count_container_40GP: '${count_container_40GP}'
            - count_container_40HC: '${count_container_40HC}'
            - Code_Header: '${Code_Header}'
            - IMPChargeTable_ExcelPath: '${IMPChargeTable_ExcelPath}'
            - IMPChargeTable_SheetName: '${IMPChargeTable_SheetName}'
            - company: '${company}'
            - user: '${user}'
            - password:
                value: '${password}'
                sensitive: true
            - sea_freight_menu: '${sea_freight_menu}'
            - BL_type: '${BL_type}'
            - folder_pdf_report: '${folder_pdf_report}'
            - freetime_dem: '${freetime_dem}'
            - freetime_det: '${freetime_det}'
        navigate:
          - SUCCESS: sleep
          - WARNING: sleep
          - FAILURE: on_failure
    - sleep:
        do:
          io.cloudslang.base.utils.sleep:
            - seconds: '3'
        navigate:
          - SUCCESS: SUCCESS
          - FAILURE: on_failure
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      ForwardPro_CreateInvoice:
        x: 100
        'y': 150
      sleep:
        x: 400
        'y': 152
        navigate:
          2f21bd56-29ee-0160-4e37-067bc139d504:
            targetId: 190d6a8f-6856-bccf-389d-575a58b42156
            port: SUCCESS
    results:
      SUCCESS:
        190d6a8f-6856-bccf-389d-575a58b42156:
          x: 700
          'y': 150
