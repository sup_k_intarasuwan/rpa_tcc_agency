namespace: TCC_Workflow.Create_Invoice.RPA_Activity
operation:
  name: Excel_Update_Value
  inputs:
    - excelPath: "C:\\Temp\\SAMPLE file for IMP charge.xls"
    - sheetName: ADD
    - rowIndex: '5'
    - colIndex: '13'
    - data: TEST
  sequential_action:
    gav: 'com.microfocus.seq:TCC_Workflow.Create_Invoice.RPA_Activity.Excel_Update_Value:2.0.0'
    external: true
    skills:
      - ActiveX
      - Visual Basic
    settings:
      windows:
        active: true
  outputs:
    - return_result: '${return_result}'
    - error_message: '${error_message}'
  results:
    - SUCCESS
    - WARNING
    - FAILURE
