########################################################################################################################
#!!
#! @input excel_Path: Source of excel file.
#! @input sheet_Name: Excel sheet name.
#! @input row_Index: Row index : start with 0
#! @input col_Index: Column Index : start with 0
#! @input cell_data: Data input.
#!!#
########################################################################################################################
namespace: TCC_Workflow.Create_Invoice.RPA_Activity
flow:
  name: Write_Excel_Data_Flow
  inputs:
    - excel_Path: "C:\\Temp\\SAMPLE file for IMP charge.xls"
    - sheet_Name: ADD
    - row_Index: '5'
    - col_Index: '13'
    - cell_data: '1234'
  workflow:
    - Excel_Update_Value:
        do:
          TCC_Workflow.Create_Invoice.RPA_Activity.Excel_Update_Value:
            - excelPath: '${excel_Path}'
            - sheetName: '${sheet_Name}'
            - rowIndex: '${row_Index}'
            - colIndex: '${col_Index}'
            - data: '${cell_data}'
        navigate:
          - SUCCESS: sleep
          - WARNING: sleep
          - FAILURE: on_failure
    - sleep:
        do:
          io.cloudslang.base.utils.sleep:
            - seconds: '3'
        navigate:
          - SUCCESS: SUCCESS
          - FAILURE: on_failure
  results:
    - SUCCESS
    - FAILURE
extensions:
  graph:
    steps:
      Excel_Update_Value:
        x: 100
        'y': 150
      sleep:
        x: 400
        'y': 150
        navigate:
          34e62690-a73f-c703-325f-d02e1c4b9e82:
            targetId: 68bfe677-f378-0745-cf9e-def2ac27cf43
            port: SUCCESS
    results:
      SUCCESS:
        68bfe677-f378-0745-cf9e-def2ac27cf43:
          x: 700
          'y': 150
