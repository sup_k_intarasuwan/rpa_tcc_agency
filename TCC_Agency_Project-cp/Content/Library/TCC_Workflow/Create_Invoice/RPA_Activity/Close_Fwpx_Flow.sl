namespace: TCC_Workflow.Create_Invoice.RPA_Activity
flow:
  name: Close_Fwpx_Flow
  workflow:
    - Close_Fwpx:
        do:
          TCC_Workflow.Create_Invoice.RPA_Activity.Close_Fwpx: []
        navigate:
          - SUCCESS: SUCCESS
          - WARNING: SUCCESS
          - FAILURE: on_failure
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      Close_Fwpx:
        x: 152.01705932617188
        'y': 126.31533813476562
        navigate:
          69515a40-c2d3-89b6-f622-74db94935c40:
            targetId: ab653c01-4541-8ae1-1b27-ac39bb861e2f
            port: SUCCESS
          d7d15956-9f75-6f19-ca89-23a202ed4fed:
            targetId: ab653c01-4541-8ae1-1b27-ac39bb861e2f
            port: WARNING
    results:
      SUCCESS:
        ab653c01-4541-8ae1-1b27-ac39bb861e2f:
          x: 338
          'y': 123
