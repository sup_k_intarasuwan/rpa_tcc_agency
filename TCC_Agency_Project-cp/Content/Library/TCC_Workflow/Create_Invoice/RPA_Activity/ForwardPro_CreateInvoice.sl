namespace: TCC_Workflow.Create_Invoice.RPA_Activity
operation:
  name: ForwardPro_CreateInvoice
  inputs:
    - BL_number: JJCSHBKA113170
    - qty_20f: '5'
    - qty_40f: '0'
    - Row_Value: ',y,y,,,,,1,y,,,y'
    - count_container_20GP: '0'
    - count_container_20TK: '0'
    - count_container_40GP: '0'
    - count_container_40HC: '1'
    - Code_Header: 'S0001,S0002,S0150,S0263,S0004,S0005,S0162,S0006,S0007,S0064,S0011,S0009'
    - IMPChargeTable_ExcelPath: "\\\\tccsrv4.tccs.co.th\\tcc_file\\SAMPLE file for IMP charge.xls"
    - IMPChargeTable_SheetName: IMP charge
    - company
    - user
    - password:
        sensitive: true
        default: '1234'
    - sea_freight_menu
    - BL_type: Original
    - folder_pdf_report: "\\\\tccsrv4.tccs.co.th\\tcc_file\\pdf\\20210809"
    - freetime_dem: '5'
    - freetime_det: '3'
  sequential_action:
    gav: 'com.microfocus.seq:TCC_Workflow.Create_Invoice.RPA_Activity.ForwardPro_CreateInvoice:2.0.0'
    external: true
    skills:
      - ActiveX
      - Visual Basic
    settings:
      windows:
        active: false
  outputs:
    - Output_Value:
        robot: true
        value: '${Output_Value}'
    - return_result: '${return_result}'
    - error_message: '${error_message}'
  results:
    - SUCCESS
    - WARNING
    - FAILURE
