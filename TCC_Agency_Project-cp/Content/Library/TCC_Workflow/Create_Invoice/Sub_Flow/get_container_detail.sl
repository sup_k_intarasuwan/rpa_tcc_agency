namespace: TCC_Workflow.Create_Invoice.Sub_Flow
flow:
  name: get_container_detail
  inputs:
    - Cnrt_List_Path: "E:\\\\TCC_File\\\\sfjobctnrlist.xls"
    - BL_Number: JJCSHLCA123162
    - Qty_20F: '1'
    - Qty_40F: '2'
    - Cntr_List_SheetName: Sheet1
  workflow:
    - get_data_from_filedowload:
        do:
          io.cloudslang.base.excel.get_cell:
            - excel_file_name: '${Cnrt_List_Path}'
            - worksheet_name: '${Cntr_List_SheetName}'
            - has_header: 'yes'
            - first_row_index: '5'
            - row_delimiter: '|'
            - column_delimiter: ','
            - BL_Number: '${BL_Number}'
        publish:
          - data: '${return_result[15:]}'
          - header
          - return_code
        navigate:
          - SUCCESS: Check_BL_inCntrList_1
          - FAILURE: on_failure
    - is_true:
        do:
          io.cloudslang.base.utils.is_true:
            - bool_value: '${BL_Macth}'
        publish: []
        navigate:
          - 'TRUE': Count_Qty_Cntr_inCntrList_1
          - 'FALSE': FAILURE
    - Check_BL_inCntrList_1:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.Operation.Check_BL_inCntrList:
            - BL_Number: '${BL_Number}'
            - Data_in_CntrList: '${data}'
        publish:
          - BL_Macth
          - Data_Select_BL_inCntrList
        navigate:
          - SUCCESS: is_true
    - Count_Qty_Cntr_inCntrList_1:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.Operation.Count_Qty_Cntr_inCntrList:
            - Data_Select_BL_inCntrList: '${Data_Select_BL_inCntrList}'
        publish:
          - Count_Cntr_Size_20
          - Count_Cntr_Size_40
          - Count_Cntr_20GP
          - Count_Cntr_20TK
          - Count_Cntr_40GP
          - Count_Cntr_40HC
        navigate:
          - SUCCESS: SUCCESS
  outputs:
    - Count_Cntr_Size_20: '${Count_Cntr_Size_20}'
    - Count_Cntr_Size_40: '${Count_Cntr_Size_40}'
    - Count_Cntr_20GP: '${Count_Cntr_20GP}'
    - Count_Cntr_20TK: '${Count_Cntr_20TK}'
    - Count_Cntr_40GP: '${Count_Cntr_40GP}'
    - Count_Cntr_40HC: '${Count_Cntr_40HC}'
  results:
    - SUCCESS
    - FAILURE
extensions:
  graph:
    steps:
      get_data_from_filedowload:
        x: 110
        'y': 112
      is_true:
        x: 458
        'y': 124
        navigate:
          d89db440-6409-5853-d9e2-1b034eb22a8f:
            targetId: 5f7d8173-6354-c28d-061a-af5754ca27d6
            port: 'FALSE'
      Count_Qty_Cntr_inCntrList_1:
        x: 646
        'y': 37
        navigate:
          ff3510c7-cff0-d922-1702-b5893ddd79cd:
            targetId: 9314c2c1-0991-90da-5fb4-a937d95c765e
            port: SUCCESS
      Check_BL_inCntrList_1:
        x: 278
        'y': 115
    results:
      SUCCESS:
        9314c2c1-0991-90da-5fb4-a937d95c765e:
          x: 835
          'y': 35
      FAILURE:
        5f7d8173-6354-c28d-061a-af5754ca27d6:
          x: 641
          'y': 240
