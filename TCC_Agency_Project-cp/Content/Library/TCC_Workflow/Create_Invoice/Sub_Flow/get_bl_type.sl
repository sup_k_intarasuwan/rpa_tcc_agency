namespace: TCC_Workflow.Create_Invoice.Sub_Flow
flow:
  name: get_bl_type
  inputs:
    - Telex_Path: "\\\\tccsrv4.tccs.co.th\\tcc_file\\G. BOX 2128S TLX.XLSX"
    - BL_Number: JJCSHLCA123190
    - Telex_SheetName: Sheet
  workflow:
    - get_data_from_telexfile:
        do:
          io.cloudslang.base.excel.get_cell:
            - excel_file_name: '${Telex_Path}'
            - worksheet_name: '${Telex_SheetName}'
            - has_header: 'yes'
            - first_row_index: '0'
            - row_delimiter: '|'
            - column_delimiter: ','
        publish:
          - data: '${return_result}'
          - header
        navigate:
          - SUCCESS: Check_BL_Type
          - FAILURE: on_failure
    - Check_BL_Type:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.Operation.Check_BL_Type:
            - Data_in_TelexFile: '${data}'
            - BL_Number: '${BL_Number}'
        publish:
          - BL_Type
        navigate:
          - SUCCESS: SUCCESS
  outputs:
    - BL_Type: '${BL_Type}'
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      get_data_from_telexfile:
        x: 147
        'y': 159
      Check_BL_Type:
        x: 325
        'y': 160
        navigate:
          e822c76f-7cc3-f461-a44d-b821cbbc3ceb:
            targetId: 2bc1b306-8f91-477a-62ea-e5d9c3cb8209
            port: SUCCESS
    results:
      SUCCESS:
        2bc1b306-8f91-477a-62ea-e5d9c3cb8209:
          x: 547
          'y': 159
