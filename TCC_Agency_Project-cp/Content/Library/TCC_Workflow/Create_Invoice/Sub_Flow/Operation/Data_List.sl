namespace: TCC_Workflow.Create_Invoice.Sub_Flow.Operation
operation:
  name: Data_List
  inputs:
    - Data_inIMP
  python_action:
    use_jython: false
    script: |-
      def execute(Data_inIMP):
          Data_List = Data_inIMP
          return{'Data_List':Data_List}
  outputs:
    - Data_List: '${Data_List}'
  results:
    - SUCCESS
