namespace: TCC_Workflow.Create_Invoice.Sub_Flow.Operation
operation:
  name: Get_MonthNumber
  inputs:
    - MonthName: August
  python_action:
    use_jython: false
    script: |-
      def execute(MonthName):
          if str(MonthName) == 'January':
              monthNumber = '01'
          elif str(MonthName) == 'February':
              monthNumber = '02'
          elif str(MonthName) == 'March':
              monthNumber = '03'
          elif str(MonthName) == 'April':
              monthNumber = '04'
          elif str(MonthName) == 'May':
              monthNumber = '05'
          elif str(MonthName) == 'June':
              monthNumber = '06'
          elif str(MonthName) == 'July':
              monthNumber = '07'
          elif str(MonthName) == 'August':
              monthNumber = '08'
          elif str(MonthName) == 'September':
              monthNumber = '09'
          elif str(MonthName) == 'October':
              monthNumber = '10'
          elif str(MonthName) == 'November':
              monthNumber = '11'
          elif str(MonthName) == 'December':
              monthNumber = '12'
          else:
              monthNumber = str(MonthName)

          return { 'monthNumber': monthNumber }
  outputs:
    - monthNumber: '${monthNumber}'
  results:
    - SUCCESS
