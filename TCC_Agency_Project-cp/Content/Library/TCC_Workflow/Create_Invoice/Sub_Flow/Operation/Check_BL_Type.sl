namespace: TCC_Workflow.Create_Invoice.Sub_Flow.Operation
operation:
  name: Check_BL_Type
  inputs:
    - Data_in_TelexFile
    - BL_Number
  python_action:
    use_jython: false
    script: "def execute(Data_in_TelexFile, BL_Number):\n    Select_BL_inTelexFile = [row for row in Data_in_TelexFile.split('|') if row.find(BL_Number)!=(-1)]\n    if len(Select_BL_inTelexFile) == 0:\n        BL_Type = 'Original'\n    else:\n        BL_Type = 'Surrender'\n       \n    return {'BL_Type':BL_Type}"
  outputs:
    - BL_Type: '${BL_Type}'
  results:
    - SUCCESS
