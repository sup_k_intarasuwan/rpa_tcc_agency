namespace: TCC_Workflow.Create_Invoice.Sub_Flow.Operation
operation:
  name: Check_BL_inCntrList
  inputs:
    - BL_Number
    - Data_in_CntrList
  python_action:
    use_jython: false
    script: "def execute(Data_in_CntrList, BL_Number):\n    Select_BL_inCntrList = [row for row in Data_in_CntrList.split('|,,,,,,,,,,,,,,|') if row.find(BL_Number)!=(-1)]\n    if len(Select_BL_inCntrList) == 0:\n        BL_Macth = 'False'\n    else:\n        BL_Macth = 'True'\n    \n    Data_Select_BL_inCntrList = '|'.join(Select_BL_inCntrList)   \n    return {'BL_Macth':BL_Macth, 'Data_Select_BL_inCntrList':Data_Select_BL_inCntrList}"
  outputs:
    - BL_Macth: '${BL_Macth}'
    - Data_Select_BL_inCntrList: '${Data_Select_BL_inCntrList}'
  results:
    - SUCCESS
