namespace: TCC_Workflow.Create_Invoice.Sub_Flow.Operation
operation:
  name: Count_Qty_Cntr_inCntrList
  inputs:
    - Data_Select_BL_inCntrList
  python_action:
    use_jython: false
    script: "def execute(Data_Select_BL_inCntrList):\n    \n    Data_Count_Cntr_Size = [row.split(',')[2].split('\\'')[0] for row in Data_Select_BL_inCntrList.split('|')]\n    Count_Cntr_Size_20 = Data_Count_Cntr_Size.count(\"20\")\n    Count_Cntr_Size_40 = Data_Count_Cntr_Size.count(\"40\")\n    \n    Data_Count_Cntr_Type = [row.split(',')[2] for row in Data_Select_BL_inCntrList.split('|')]\n    Count_Cntr_20GP = Data_Count_Cntr_Type.count(\"20'GP\")\n    Count_Cntr_20TK = Data_Count_Cntr_Type.count(\"20'TK\")\n    Count_Cntr_40GP = Data_Count_Cntr_Type.count(\"40'GP\")\n    Count_Cntr_40HC = Data_Count_Cntr_Type.count(\"40'HC\")\n\n    return {'Count_Cntr_Size_20':Count_Cntr_Size_20, \n            'Count_Cntr_Size_40':Count_Cntr_Size_40, \n            'Count_Cntr_20GP':Count_Cntr_20GP, \n            'Count_Cntr_20TK':Count_Cntr_20TK, \n            'Count_Cntr_40GP':Count_Cntr_40GP, \n            'Count_Cntr_40HC':Count_Cntr_40HC}"
  outputs:
    - Count_Cntr_Size_20: '${Count_Cntr_Size_20}'
    - Count_Cntr_Size_40: '${Count_Cntr_Size_40}'
    - Count_Cntr_20GP: '${Count_Cntr_20GP}'
    - Count_Cntr_20TK: '${Count_Cntr_20TK}'
    - Count_Cntr_40GP: '${Count_Cntr_40GP}'
    - Count_Cntr_40HC: '${Count_Cntr_40HC}'
  results:
    - SUCCESS
