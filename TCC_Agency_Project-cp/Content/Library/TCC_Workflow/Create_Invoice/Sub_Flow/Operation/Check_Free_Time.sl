namespace: TCC_Workflow.Create_Invoice.Sub_Flow.Operation
operation:
  name: Check_Free_Time
  inputs:
    - Data_FreeTime_File1
    - BL_Number
    - Header
    - File
    - Default_DEM
    - Default_DET
  python_action:
    use_jython: false
    script: "def execute(Data_FreeTime_File1, BL_Number, Header, File, Default_DEM, Default_DET):\n    if File == '1':\n        Select_BL_inFreeTimeFile = [row for row in Data_FreeTime_File1.split('|') if row.find(BL_Number)!=(-1)]\n        if len(Select_BL_inFreeTimeFile) == 0:\n            Free_Time = 'False'\n            DEM = '0'\n            DET = '0'\n        else:\n            Free_Time = 'True'\n            DEM = Select_BL_inFreeTimeFile[0].split(',')[Header.split(\",\").index('DEM')]\n            DET = Select_BL_inFreeTimeFile[0].split(',')[Header.split(\",\").index('DET')]\n            DEM = DEM.split('.')[0]\n            DET = DET.split('.')[0]\n    elif File == '2':\n        Select_BL_inFreeTimeFile = [row for row in Data_FreeTime_File1.split('|') if row.find(BL_Number)!=(-1)]\n        if len(Select_BL_inFreeTimeFile) == 0:\n            DEM = Default_DEM\n            DET = Default_DET\n            \n        else:\n            FT = Select_BL_inFreeTimeFile[0].split(';')[Header.split(';').index('FT')]\n            if FT != '' : \n                Select_FT_DEM = [row for row in FT.split(',') if row.find('DEM')!=(-1)]\n                if len(Select_FT_DEM) == 0:\n                    DEM = Default_DEM\n                else:\n                    DEM = Select_FT_DEM[0]\n                \n                Select_FT_DET = [row for row in FT.split(',') if row.find('DET')!=(-1)]\n                if len(Select_FT_DET) == 0:\n                    DET = Default_DET\n                else:\n                    DET = Select_FT_DET[0]\n            else:\n                DEM = Default_DEM\n                DET = Default_DET\n        Free_Time = 'True'\n        \n    return {'Free_Time':Free_Time,\n            'DEM':DEM,\n            'DET':DET,}"
  outputs:
    - Free_Time: '${Free_Time}'
    - DEM: '${DEM}'
    - DET: '${DET}'
  results:
    - SUCCESS
