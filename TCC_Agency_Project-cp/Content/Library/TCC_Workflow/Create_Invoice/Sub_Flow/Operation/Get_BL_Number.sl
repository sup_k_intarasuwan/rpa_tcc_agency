namespace: TCC_Workflow.Create_Invoice.Sub_Flow.Operation
operation:
  name: Get_BL_Number
  inputs:
    - Data_Row_IMP
    - Idx_BL_Number
    - Data_from_IMP_File
    - Frist_Row_Idx_IMP_File
  python_action:
    use_jython: false
    script: "def execute(Data_Row_IMP, Idx_BL_Number, Data_from_IMP_File, Frist_Row_Idx_IMP_File):\n    \n    BL_Number = Data_Row_IMP.split(',')[int(Idx_BL_Number)]\n    \n    Row_Idx_Data = str(Data_from_IMP_File.split('|').index(Data_Row_IMP))\n    \n    Row_Idx_inExcel = str(int(Frist_Row_Idx_IMP_File)+int(Row_Idx_Data)+1)\n    \n    return {'BL_Number':BL_Number, \n            'Row_Idx_Data':Row_Idx_Data, \n            'Row_Idx_inExcel':Row_Idx_inExcel}"
  outputs:
    - BL_Number: '${BL_Number}'
    - Row_Idx_Data: '${Row_Idx_Data}'
    - Row_Idx_inExcel: '${Row_Idx_inExcel}'
  results:
    - SUCCESS
