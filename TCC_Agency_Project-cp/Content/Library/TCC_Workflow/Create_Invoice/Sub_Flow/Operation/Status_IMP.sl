namespace: TCC_Workflow.Create_Invoice.Sub_Flow.Operation
operation:
  name: Status_IMP
  inputs:
    - Status
  python_action:
    use_jython: false
    script: |-
      def execute(Status):
          if Status == 'Fail BL Mismatch':
              Status_Discription = 'Fail: BL Mismatch in Container List File'
          elif Status == 'Fail Qty Mismatch':
              Status_Discription = 'Fail: Container Quantity of IMP File and Container List File Mismatch '
          elif Status == 'Fail Forwardpro':
              Status_Discription = 'Fail: Forwardpro Fail'
          else:
              Status_Discription = 'Pass'
          return{'Status_Discription':Status_Discription}
  outputs:
    - Status_Discription: '${Status_Discription}'
  results:
    - SUCCESS
