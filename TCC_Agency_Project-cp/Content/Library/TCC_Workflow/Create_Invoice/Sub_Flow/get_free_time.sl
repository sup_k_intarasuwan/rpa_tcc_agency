namespace: TCC_Workflow.Create_Invoice.Sub_Flow
flow:
  name: get_free_time
  inputs:
    - FreeTime_File1_Path: "\\\\tccsrv4.tccs.co.th\\tcc_file\\G. BOX 2128S -TH.xlsx"
    - FreeTime_File1_SheetName: Sheet1
    - FreeTime_File2_Path: "\\\\tccsrv4.tccs.co.th\\tcc_file\\G. BOX 2128S ADD.xlsx"
    - FreeTime_File2_SheetName: Sheet
    - BL_Number: JJCSHBKA122661
    - Default_DEM: '5'
    - Default_DET: '3'
  workflow:
    - get_data_from_telexfile:
        do:
          io.cloudslang.base.excel.get_cell:
            - excel_file_name: '${FreeTime_File1_Path}'
            - worksheet_name: '${FreeTime_File1_SheetName}'
            - has_header: 'yes'
            - first_row_index: '1'
            - row_delimiter: '|'
            - column_delimiter: ','
        publish:
          - Data_File1: '${return_result}'
          - Header_File1: '${header}'
        navigate:
          - SUCCESS: Check_Free_Time_1
          - FAILURE: on_failure
    - is_true:
        do:
          io.cloudslang.base.utils.is_true:
            - bool_value: '${Free_Time}'
        publish: []
        navigate:
          - 'TRUE': SUCCESS
          - 'FALSE': get_data_from_telexfile_1
    - get_data_from_telexfile_1:
        do:
          io.cloudslang.base.excel.get_cell:
            - excel_file_name: '${FreeTime_File2_Path}'
            - worksheet_name: '${FreeTime_File2_SheetName}'
            - has_header: 'yes'
            - first_row_index: '4'
            - row_delimiter: '|'
            - column_delimiter: ;
        publish:
          - Data_File2: '${return_result}'
          - Header_File2: '${header}'
        navigate:
          - SUCCESS: Check_Free_Time_2
          - FAILURE: on_failure
    - Check_Free_Time_1:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.Operation.Check_Free_Time:
            - Data_FreeTime_File1: '${Data_File1}'
            - BL_Number: '${BL_Number}'
            - Header: '${Header_File1}'
            - File: '1'
            - Default_DEM: '${Default_DEM}'
            - Default_DET: '${Default_DET}'
        publish:
          - Free_Time
          - DEM
          - DET
        navigate:
          - SUCCESS: is_true
    - Check_Free_Time_2:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.Operation.Check_Free_Time:
            - Data_FreeTime_File1: '${Data_File2}'
            - BL_Number: '${BL_Number}'
            - Header: '${Header_File2}'
            - File: '2'
            - Default_DEM: '${Default_DEM}'
            - Default_DET: '${Default_DET}'
        publish:
          - DEM
          - DET
        navigate:
          - SUCCESS: SUCCESS
  outputs:
    - DEM: '${DEM}'
    - DET: '${DET}'
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      get_data_from_telexfile:
        x: 8
        'y': 161
      Check_Free_Time_1:
        x: 160
        'y': 166
      is_true:
        x: 351
        'y': 166
        navigate:
          f7435650-4878-8de2-0c9c-a1ce62eea832:
            targetId: 00dcbae1-f8af-9ef4-ee5d-89d8116ef2c9
            port: 'TRUE'
      get_data_from_telexfile_1:
        x: 352
        'y': 359
      Check_Free_Time_2:
        x: 531
        'y': 364
        navigate:
          778b8c46-32c3-e9f7-4d80-36e275dbff80:
            targetId: 00dcbae1-f8af-9ef4-ee5d-89d8116ef2c9
            port: SUCCESS
    results:
      SUCCESS:
        00dcbae1-f8af-9ef4-ee5d-89d8116ef2c9:
          x: 525
          'y': 168
