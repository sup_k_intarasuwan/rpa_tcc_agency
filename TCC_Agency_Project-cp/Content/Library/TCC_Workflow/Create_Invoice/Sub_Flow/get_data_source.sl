########################################################################################################################
#!!
#! @description: Get datasource from excel file.
#!
#! @input excel_data_source: Source excel file for create invoice.
#! @input excel_data_sheetname: Sheet Data for create invoice.
#!!#
########################################################################################################################
namespace: TCC_Workflow.Create_Invoice.Sub_Flow
flow:
  name: get_data_source
  inputs:
    - excel_data_source: "\\\\tccsrv4.tccs.co.th\\tcc_file\\SAMPLE file for IMP charge.xls"
    - excel_data_sheetname: ADD
  workflow:
    - get_data_ADDsheet:
        do:
          io.cloudslang.base.excel.get_cell:
            - excel_file_name: '${excel_data_source}'
            - worksheet_name: '${excel_data_sheetname}'
            - has_header: 'yes'
            - first_row_index: '3'
            - row_delimiter: '|'
            - column_delimiter: ','
        publish:
          - header
          - data: '${return_result}'
          - Idx_BL_Number: "${str(header.split(\",\").index('B/L NO'))}"
          - Idx_Port_Discharge: "${str(header.split(\",\").index('Port of Discharge'))}"
          - Idx_20F: "${str(header.split(\",\").index('20F'))}"
          - Idx_40F: "${str(header.split(\",\").index('40F'))}"
          - Idx_HDS: "${str(header.split(\",\").index('HDS'))}"
          - Idx_Contract_Code: "${str(header.split(\",\").index('CONTRACT CODE'))}"
          - Idx_FT: "${str(header.split(\",\").index('FT'))}"
          - Idx_Term: "${str(header.split(\",\").index('TERM'))}"
          - Idx_Transship: "${str(header.split(\",\").index('TRANSSHIP'))}"
          - Idx_Status: "${str(header.split(\",\").index('STATUS'))}"
          - Service_Code_Idx_Start: '14'
          - Service_Code_Idx_End: '${str(int(columns_count) - 1)}'
          - first_row_index
          - Idx_RPA_Status: "${str(header.split(\",\").index('RPA_STATUS'))}"
        navigate:
          - SUCCESS: SUCCESS
          - FAILURE: on_failure
  outputs:
    - header_data_source: '${header}'
    - details_data_source: '${data}'
    - Idx_First_Row: '${first_row_index}'
    - Idx_BL_Number: '${Idx_BL_Number}'
    - Idx_Port_Discharge: '${Idx_Port_Discharge}'
    - Idx_20F: '${Idx_20F}'
    - Idx_40F: '${Idx_40F}'
    - Idx_HDS: '${Idx_HDS}'
    - Idx_Contract_Code: '${Idx_Contract_Code}'
    - Idx_FT: '${Idx_FT}'
    - Idx_Term: '${Idx_Term}'
    - Idx_Transship: '${Idx_Transship}'
    - Idx_Status: '${Idx_Status}'
    - Idx_RPA_Status: '${Idx_RPA_Status}'
    - Idx_Service_Code_Start: '${Service_Code_Idx_Start}'
    - Idx_Service_Code_End: '${Service_Code_Idx_End}'
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      get_data_ADDsheet:
        x: 100
        'y': 150
        navigate:
          32218feb-0cde-70a2-c646-098666bcd2de:
            targetId: 201fd1e4-3417-4dc7-bb4a-c255fefe66a5
            port: SUCCESS
    results:
      SUCCESS:
        201fd1e4-3417-4dc7-bb4a-c255fefe66a5:
          x: 400
          'y': 150
