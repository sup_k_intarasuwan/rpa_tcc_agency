namespace: TCC_Workflow.Create_Invoice.Sub_Flow
flow:
  name: create_folder_pdf_files
  workflow:
    - get_date:
        do:
          io.cloudslang.base.datetime.get_time: []
        publish:
          - output
          - day: "${str('0'+str(output).split(' ')[1][:-1])[-2:]}"
          - month: "${str(output).split(' ')[0]}"
          - year: "${str(output).split(' ')[2]}"
          - time: "${str(output).split(' ')[3]}"
        navigate:
          - SUCCESS: Get_MonthNumber
          - FAILURE: on_failure
    - create_folder:
        do:
          io.cloudslang.base.filesystem.create_folder:
            - folder_name: '${folderName}'
        publish:
          - folder_name
        navigate:
          - SUCCESS: SUCCESS
          - FAILURE: SUCCESS
    - Get_MonthNumber:
        do:
          TCC_Workflow.Create_Invoice.Sub_Flow.Operation.Get_MonthNumber:
            - MonthName: '${month}'
            - year: '${year}'
            - day: '${day}'
        publish:
          - monthNumber
          - folderName: "${'\\\\'+'\\\\tccsrv4.tccs.co.th\\\\tcc_file\\\\pdf\\\\' + year + monthNumber + day}"
        navigate:
          - SUCCESS: create_folder
  outputs:
    - folderName: '${folder_name}'
  results:
    - FAILURE
    - SUCCESS
extensions:
  graph:
    steps:
      get_date:
        x: 40
        'y': 77
      create_folder:
        x: 218
        'y': 254
        navigate:
          84e9337f-c156-ffd9-0ca8-335eccc491a1:
            targetId: df3c4037-e3a5-5fbb-96f9-ede74c9f90f1
            port: SUCCESS
          de070203-c6bd-5598-fcb8-ae8cf443f615:
            targetId: df3c4037-e3a5-5fbb-96f9-ede74c9f90f1
            port: FAILURE
      Get_MonthNumber:
        x: 217
        'y': 79
    results:
      SUCCESS:
        df3c4037-e3a5-5fbb-96f9-ede74c9f90f1:
          x: 465
          'y': 219
