namespace: TCC_Workflow.Create_Invoice.Sub_Flow
flow:
  name: get_fso_child
  workflow:
    - get_children:
        do:
          io.cloudslang.base.filesystem.get_children:
            - source: "E:\\\\TCC_File\\\\FSO_Check"
            - delimiter: ;
        publish:
          - return_result
          - bool_cnt: "${'true' if int(count) == 1 else 'false'}"
        navigate:
          - SUCCESS: get_time
          - FAILURE: on_failure
    - move:
        do:
          io.cloudslang.base.filesystem.move:
            - source: '${return_result}'
            - destination: "E:\\\\TCC_File\\\\FSO_Moved\\\\"
        navigate:
          - SUCCESS: SUCCESS
          - FAILURE: on_failure
    - is_true:
        do:
          io.cloudslang.base.utils.is_true:
            - bool_value: '${bool_cnt}'
        navigate:
          - 'TRUE': move
          - 'FALSE': SUCCESS
    - get_time:
        do:
          io.cloudslang.base.datetime.get_time: []
        publish:
          - output
          - day: "${str(output).split(' ')[1][:-1]}"
          - month: "${str(output).split(' ')[0]}"
          - year: "${str(output).split(' ')[3]}"
          - time: "${str(output).split(' ')[4]}"
        navigate:
          - SUCCESS: is_true
          - FAILURE: on_failure
  outputs:
    - fso_count_child: '${bool_cnt}'
    - return_result: '${return_result}'
  results:
    - SUCCESS
    - FAILURE
extensions:
  graph:
    steps:
      get_children:
        x: 37
        'y': 105
      move:
        x: 500
        'y': 290
        navigate:
          4f2f3de0-fb7f-39a7-49ff-3954a46930ea:
            targetId: 2c8274f2-1d90-f7d8-0de0-91c6e64384c9
            port: SUCCESS
      is_true:
        x: 324
        'y': 117
        navigate:
          633e150f-df84-a661-4748-2888b6f2cca4:
            targetId: 2c8274f2-1d90-f7d8-0de0-91c6e64384c9
            port: 'FALSE'
      get_time:
        x: 183
        'y': 257
    results:
      SUCCESS:
        2c8274f2-1d90-f7d8-0de0-91c6e64384c9:
          x: 659
          'y': 116
